from API_Keys import *
import os
import logging
import tweepy
import sys


logger = logging.getLogger()
logging.basicConfig(level=logging.INFO)


def create_api():
    consumer_key = API_Key
    consumer_secret = API_secret_key
    access_token = Access_token
    access_token_secret = Access_token_secret

    auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    api = tweepy.API(auth, wait_on_rate_limit=True,
                     wait_on_rate_limit_notify=True)
    try:
        api.verify_credentials()
        logger.info("it worked")
    except Exception as e:
        logger.error("Error creating API", exc_info=True)
        raise e
    logger.info("API created")
    return api
