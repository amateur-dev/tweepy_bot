FROM python:3.7-alpine
WORKDIR /home
ADD . /home
RUN pip3 install -r requirements.txt

CMD ["python3", "favAndRetweet.py"]